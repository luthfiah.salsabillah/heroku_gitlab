from django.test import TestCase, Client

# Create your tests here.
class Testing123(TestCase):
    def test_url_home(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)
    def test_url_profile(self):
        response = Client().get('/profile/')
        self.assertEquals(response.status_code, 200)